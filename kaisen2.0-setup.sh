#!/bin/bash

#-#-#-#-#-#-#-#-#-#-#
#--Made:by:0n1cOn3--#
#                   #
#      Made in      #
#    Switzerland    #
#        <3         #
#-#-#-#-#-#-#-#-#-#-#

## Environment Config!

clear
home=$(pwd)
logfolder=$(pwd)/logs

exit_on_signal_SIGINT () {
    echo -e "\n\n\e\$[✗] Installation has been abort!\e[0m"
    exit 0
}
trap exit_on_signal_SIGINT SIGINT

## Color the shell a bit :))

White='\033[0;97m'
Green='\033[0;32m'
Red='\033[0;91m'
Reset='\033[0m'

# some functions



## Root Check

if ! [ $(id -u) = 0 ]; then
   	clear
   	echo -e " ${Red}*****************************************************************"
   	echo -e " ${Red}* ${Green}Kaisen2.0 Setup requires to be executed with root permission!${Red} *"
   	echo -e " ${Red}*****************************************************************${Reset}"
   	sleep 0.5
   	clear
  	exit 0
	else
   	echo -e " ${Red}**********************************"
   	echo -e " ${Red}* ${Green}Executed with root permission!"
   	echo -e " ${Red}**********************************"
   	sleep 1
fi

## Preparing System:

clear
echo -e " ${Red}****************************************************"
echo -e " ${Red}* ${Green}Preparing System to continue OS Installation${Red}"
echo -e " ${Red}****************************************************${Reset}"
sleep 2
echo -e ${Reset}

if ! hash git 2>/dev/null;then
	echo -e $Red "Not Installed [✗]"
	echo -e ${Reset}
	sudo apt install git -y 2>&1 | tee gitsetup.log
else
	echo -e $Green " git installed [✓]"
   echo -e ${Reset}
   sleep 2
fi
echo -e ${Reset}

if ! hash wget 2>/dev/null;then
	echo -e $Red "Not Installed [✗]"
   echo -e ${Reset} 
   sudo apt install wget -y 2>&1 | tee wgetsetup.log
else
	echo -e $Green "wget installed [✓]"
   echo -e ${Reset}
   sleep 2
fi

if ! hash iotop 2>/dev/null;then
        echo -e $Red "fail2ban not Installed [✗]"
        echo -e ${Reset}
        apt install iotop -y 2>&1 | tee iotopsetup.log
else
        echo -e $Green "fail2ban installed [✓]"
        echo -e ${Reset}
        sleep 2
fi
echo -e ${Reset}
clear

## Update Sources & Upgrade to the lastest Packages

clear
echo -e " ${Red}***************************************************************"
echo -e " ${Red}* ${Green}Update source and upgrade to the latest OS packages ${Red}*"
echo -e " ${Red}***************************************************************"
sleep 2
echo -e ${Reset}
apt update -y > /dev/null | tee error.log
apt upgrade -y 2>&1 | tee upgradelog.log
clear
echo -e " ${Red}*************************************************************"
echo -e " ${Red}* ${Green}Preparing System to continue OS Installation ${Red}*"
echo -e " ${Red}*************************************************************"
sleep 2
echo -e ${Reset}

if ! hash fail2ban 2>/dev/null;then
	echo -e $Red "fail2ban not Installed [✗]"
	echo -e ${Reset}
	apt install fail2ban -y 2>&1 | tee fail2bansetup.log
else
	echo -e $Green "fail2ban installed [✓]"
   	echo -e ${Reset}
   	sleep 2
fi
echo -e ${Reset}

if ! hash htop 2>/dev/null;then
        echo -e $Red "fail2ban not Installed [✗]"
        echo -e ${Reset}
        apt install htop -y 2>&1 | tee htopsetup.log
else
        echo -e $Green "fail2ban installed [✓]"
        echo -e ${Reset}
        sleep 2
fi
echo -e ${Reset}


#echo -e " ${Red}****************************************************"
#echo -e " ${Red}* ${Green}local package required...downloading... ${Red}*"
#echo -e " ${Red}****************************************************"
#sleep 2
## locaal package required: #dpkg -i anydesk_6.1.0-1_amd64.deb

if ! hash anydesk 2>/dev/null;then
	echo -e $Red "Not Installed [✗]"
	echo -e ${Reset}
	wget https://download.anydesk.com/linux/anydesk_6.1.0-1_amd64.deb 2>&1 | tee addlerror.log
   	sudo dpkg -i anydesk_6.1.0-1_amd64.deb 2>&1 | tee adsetup.txt
   	sudo apt install -f
	rm anydesk_6.1.0-1_amd64.deb
else
	echo -e $Green "Anydesk installed [✓]"
   	echo -e ${Reset}
   	sleep 2
fi

clear
echo -e ${Reset}

## install Github Desktop

if ! hash gdebi 2>/dev/null;then
	echo -e $Red "gdebi not Installed [✗]"
	sudo apt-get install gdebi-core -y 2>&1 | tee gdebisetup.log
else
	echo -e $Green "gdebi Installed [✓]"
   	echo -e ${Reset}
   	sleep 2
fi

if ! hash github-desktop 2>/dev/null;then
	echo -e $Red "Github-Desktop not Installed [✗]"
	wget https://github.com/shiftkey/desktop/releases/download/release-2.6.3-linux1/GitHubDesktop-linux-2.6.3-linux1.deb 
    sudo gdebi GitHubDesktop-linux-2.6.3-linux1.deb 2>&1 | tee ghgdebisetup.log
    rm gdebi GitHubDesktop-linux-2.6.3-linux1.deb
else
	echo -e $Green "Github Desktop Installed [✓]"
   	echo -e ${Reset}
   	sleep 2
fi

clear
sudo apt update
sudo apt upgrade -y
echo -e ${Reset}

## Snap Installations...

echo -e " ${Red}*************************"
echo -e " ${Red}* ${Green}Snap Installations... ${Red}*"
echo -e " ${Red}*************************"
sleep 2
echo -e ${Reset}
sudo snap install code --classic 2>&1 | tee snapcode.log
#snap install icq-im
sudo snap install telegram-desktop 2>&1 | tee snaptele.log
#snap install handbrake-jz
sudo snap install mailspring 2>&1 | tee snapmail.log
#snap install bitwarden
sudo snap install bitwarden 2>&1 | tee snapbit.log
clear
echo -e "Installation ist beendet..."
echo ""
sleep 3
clear
whoami
ls 
